from django.db import models
from datetime import datetime
# Create your models here.
class myschedule(models.Model):
	nama = models.CharField(max_length=68)
	tanggal = models.DateField()
	jam = models.TimeField()
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=100)

	def __str__(self):
	   return"{}".format(self.nama)
