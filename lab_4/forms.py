from django import forms
from .models import myschedule
from django.forms import ModelForm

class add_form(forms.ModelForm):
	class Meta:
		model = myschedule
		fields = ["nama","tanggal", "jam", "tempat","kategori"]
		widgets = {
			"nama" : forms.TextInput(attrs={'class' : 'form-control'}),
			"tanggal" : forms.TextInput(attrs={'type':'date' ,'class' : 'form-control'}),
			"jam" : forms.TextInput(attrs={'type':'time' ,'class' : 'form-control'}),
			"kategori" : forms.TextInput(attrs={'class' : 'form-control'}),
			"tempat" : forms.TextInput(attrs={'class' : 'form-control'}),
		}