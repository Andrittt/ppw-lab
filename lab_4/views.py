from django.http import HttpResponse
from django.shortcuts import render
from. models import myschedule
from. forms import add_form
from django.contrib import messages
from django.http import HttpResponseRedirect

def blank(request):
	return render(request,'blank.html')

def aboutme(request):
	return render(request,'aboutme.html')

def form(request):
    return render(request,'form.html')

def Resultmyschedule(request):
    myschedules = myschedule.objects.all()
    context = {
        'myschedules': myschedules,
    }    	
    return render(request,'Resultmyschedule.html',context)

def schedule(request):
    if request.method == "POST":
    	form = add_form(request.POST)
    	if form.is_valid():
    		form_item = form.save(commit=True)
    		form_item.save()
    	return HttpResponseRedirect('/')
    else:
    	form = add_form()
    return render(request, 'myschedule.html', {'form' : form})

def Delete(request):
    myschedule.objects.all().delete()
    return HttpResponseRedirect('/Resultmyschedule')    

                	           	   
