from django.shortcuts import render
from django.http import JsonResponse
from django.urls import reverse
from django.http import HttpResponseRedirect
import urllib.request, json
import requests
import json
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt


def books(request):
	return render(request,'books.html')

@csrf_exempt
def booklist(request):
	user = request.user
	if user is not None:
		jumlahFav = 0
		if request.method == 'POST':
			request.session['jumlahFav'] = request.POST['jumlahFav']

		if request.method != 'POST':

			if user is not None:
				jumlahFav = request.session.get('jumlahFav')
				return render(request, 'books.html', {'jumlahFav': jumlahFav})
	else :
		return HttpResponseRedirect('/login/')
	return render(request, 'books.html')

def logout(request):
	request.session.flush()
	return render(request, 'books.html')


def book_data(request):
	try:
		q = request.GET['q']
	except:
		q = 'quilting'

	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ q)
	jsonparse = json.dumps(getJson.json())
	return HttpResponse(jsonparse)

# Create your views here.
