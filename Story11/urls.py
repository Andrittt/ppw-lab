from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from Story11 import views as storyviews
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',storyviews.books, name='book'),
    url(r'^login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social'))

]