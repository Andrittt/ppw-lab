from django.test import TestCase, Client
from django.urls import resolve
from .views import books


# Create your tests here.

class Story9test(TestCase):
	def test_Story11_url_is_exist(self):
	    response = Client().get('/')
	    self.assertEqual(response.status_code,200)

	def test_Story11_using_index_template(self):
	    response = Client().get('/')
	    self.assertTemplateUsed(response, 'books.html')

	def test_Story11_using_addStatus_func(self):
	    found = resolve('/')
	    self.assertEqual(found.func,books)

	def test_login(self):
		response = Client().get('/login')
		self.assertEqual(response.status_code, 301)
	
	def test_logout(self):
		response = Client().get('/logout')
		self.assertEqual(response.status_code, 301)


# Create your tests here.
