from django import forms

from .models import Subscribe
from django.forms import ModelForm


class subs_form(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Subscribe
        fields = ["email","nama","password1"]

