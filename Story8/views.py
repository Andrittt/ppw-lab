from django.shortcuts import render
from django.http import JsonResponse
from .models import Subscribe
from .forms import subs_form
from django.contrib import messages

from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError	
from django.views.decorators.csrf import csrf_exempt



def profile(request):
	return render(request,'profileku.html')

@csrf_exempt
def Subscribe_Views(request):
	all_subs = Subscribe.objects.all()
	subs_list = Subscribe.objects.all()
	if request.method == "POST":
		print(request.POST)
		form = subs_form(request.POST)
		if form.is_valid():
			form_item = form.save(commit=True)
			form_item.save()
			form = subs_form()
	else:
		form = subs_form()
	return render(request, 'subs.html', {'form' : form, 'subs_list' : subs_list})
	


@csrf_exempt
def check_email(request):
	if request.method == 'POST':
		email = request.POST['email']
		nama = request.POST['nama']
		password1 = request.POST['password1']

		subs_filter = Subscribe.objects.filter(email = email)

		if len(subs_filter) > 0 :
			return JsonResponse({'message' : 'Email already used'})

		else:
			return JsonResponse({'message' : 'User Valid'})

	else:
		return JsonResponse({'message' : 'Something went wrong'})

def subs_list(request):
	subs_list = Subscribe.objects.all()
	response['subs_list'] = subs_list
	html = 'subs.html'
	return render(request, html, response)

def subs_list_json(request):
	subs = [obj.as_dict() for obj in Subscribe.objects.all()]
	return JsonResponse({"results": subs}, content_type='application/json')

@csrf_exempt
def unsubscribe(request):
	if request.method == "POST":
		email = request.POST['email']
		Subscribe.objects.filter(email =email).delete()
		return HttpResponseRedirect('/subscribe')


# Create your views here.
