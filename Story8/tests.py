from django.test import TestCase, Client
from django.urls import resolve
from .views import profile


# Create your tests here.

class Story8test(TestCase):
	def test_Story8_url_is_exist(self):
	    response = Client().get('/')
	    self.assertEqual(response.status_code,200)

	def test_Story8_using_index_template(self):
	    response = Client().get('/')
	    self.assertTemplateUsed(response, 'profileku.html')

	def test_Story8_using_addStatus_func(self):
	    found = resolve('/')
	    self.assertEqual(found.func,profile)

	def test_8Story10_using_index_template(self):
		response = Client().get('/subscribe/')
		self.assertTemplateUsed(response, 'subs.html')


# class test_NewVisitor_Story8(unittest.TestCase):
# 	def setUp(self):
# 	    chrome_options = Options()
# 	    chrome_options.add_argument('--dns-prefetch-def test_8Story10_using_index_template(self):
# 	    response = Client().get('/subscribe/')
# 	    self.assertTemplateUsed(response, 'subs.html')

# 	def test_Story10_using_func(self):
# 	    found = resolve('/')
# 	    self.assertEqual(found.func,Home)
# disable')
# 	    chrome_options.add_argument('--no-sandbox')
# 	    chrome_options.add_argument('--headless')
# 	    chrome_options.add_argument('disable-gpu')
# 	    service_log_path = "./chromedriver.log"
# 	    service_args = ['--verbose']
# 	    self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 	    self.browser.implicitly_wait(3) 
# 	    super(test_NewVisitor_Story8,self).setUp()

# 	def tearDown(self):
# 	    self.browser.implicitly_wait(3)
# 	    self.browser.quit()

# 	def test_change_and_previous_click(self):
# 	    self.browser.get('https://ppw-b-andri-sanss.herokuapp.com/')
# 	    time.sleep(2)

# 	    self.browser.find_element_by_id('change_background2').click()
# 	    self.browser.find_element_by_id('change_background').click()
	   
# 	    time.sleep(2)

# 	def test_css(self):
# 		self.browser.get('https://ppw-b-andri-sanss.herokuapp.com/')
# 		time.sleep(1)

# 		change_button = self.browser.find_element_by_id('change_background')
# 		previous_button = self.browser.find_element_by_id('change_background2')

# 		self.assertIn('Change', change_button.text)
# 		self.assertIn('Previous', previous_button.text)

# 		activity = self.browser.find_element_by_name('activity')
# 		organization = self.browser.find_element_by_name('organization')
# 		achievement = self.browser.find_element_by_name('achievement')

# 		activity_use_class = "accordion" in activity.get_attribute("class")
# 		organization_use_class = "accordion" in organization.get_attribute("class")
# 		achievement_use_class = "accordion" in achievement.get_attribute("class")

# 		self.assertTrue(activity_use_class)
# 		self.assertTrue(organization_use_class)
# 		self.assertTrue(achievement_use_class)

if __name__ == '__main__': 
	unittest.main(warnings='ignore') 

# Create your tests here.
