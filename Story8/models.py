from django.db import models
from django.utils import timezone
from datetime import datetime


class Subscribe(models.Model):
	email = models.EmailField()
	nama = models.CharField(max_length=150)
	password1 = models.CharField(max_length=100)

	def as_dict(self):
		return{
			"email" : self.email,
			"nama" : self.nama,
			"password1" : self.password1,
		}

	def __str__(self):
		return "{}".format(self.email)

# Create your models here.
