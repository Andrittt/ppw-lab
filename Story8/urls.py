from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from Story8 import views as storyviews

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', storyviews.profile),
    url(r'^subscribe/$', storyviews.Subscribe_Views),
    url(r'^subscribe/check_email/$', storyviews.check_email),
    url(r'^get-subs-list/$', storyviews.subs_list_json),
    url(r'^subscribe/delete/$', storyviews.unsubscribe),

]