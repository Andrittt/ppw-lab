from django.http import HttpResponse
from django.shortcuts import render
from. models import Status
from. forms import add_status
from django.contrib import messages
from django.http import HttpResponseRedirect

response = {}
# def index(request):
# 	response['form'] = add_status
# 	response['StatusS'] = Status.objects.all()
# 	return render(request,'index.html', response)

def profile(request):
	return render(request,'profile.html')

def addStatus(request):
	form = add_status(request.POST or None)
	if request.method == "POST":
		formStatus = add_status(request.POST)
		if formStatus.is_valid():
			form_item = formStatus.save(commit=True)
			form_item.save()
			return HttpResponseRedirect('/')
	else:
		formStatus = add_status()
		dataStatus = Status.objects.all()
		context = {
			'StatusS': dataStatus,
		}
	return render(request, 'index.html', {'form' : formStatus, 'StatusS': dataStatus})

def deleteStatus(request):
	Status.objects.all().delete()
	return HttpResponseRedirect('/')



# Create your views here.
