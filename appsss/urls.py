from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from appsss import views as storyviews

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^isistatus/',storyviews.addStatus),
    url(r'^delete/', storyviews.deleteStatus),
    url(r'^$', storyviews.profile),

]