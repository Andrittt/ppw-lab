from django import forms
from .models import Status
from django.forms import ModelForm


class add_status(forms.ModelForm):
    class Meta:
        model = Status
        fields = ["Statusku", "isi_Status"]
        widgets = {
            "Statusku" : forms.TextInput(attrs={'class' : 'form-control'}),
            "isi_Status" : forms.TextInput(attrs={'class' : 'form-control'}),
        }