from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from .views import addStatus
from .views import profile
from .views import deleteStatus
from .models import Status
from .forms import add_status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import unittest

class Story6test(TestCase):
    def test_Story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_Story6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_Story6_using_addStatus_func(self):
        found = resolve('/')
        self.assertEqual(found.func,addStatus)
    
    # def test_model_can_create_new_status(self):
    #         new_activity = Status.objects.create(Statusku='ini judul',isi_Status='Aku mau latihan ngoding deh')
    #         counting_all_available_activity = Status.objects.all().count()
    #         self.assertEqual(counting_all_available_activity,1)

    # def test_can_save_a_POST_request(self):
    #     response = self.client.post('/addStatus', data={'Statusku': 'ini judul', 'isi_Status' : 'aku mau latihan ngoding dehh'})
    #     counting_all_available_activity = Status.objects.all().count()
    #     self.assertEqual(response.status_code, 301)
    #     self.assertEqual(response['location'], '/addStatus/')
    #     new_response = self.client.get('/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('aku mau latihan ngoding dehh', html_response)

    # def test_Story6_post_success_and_render_the_result(self):
    #         test = 'Anonymous'
    #         response_post = Client().post('/', {'Statusku': test, 'isi_Status': test})
    #         self.assertEqual(response_post.status_code, 302)
    
    #         response= Client().get('/')
    #         html_response = response.content.decode('utf8')
    #         self.assertIn(test, html_response)

    # def test_Story6_post_error_and_render_the_result(self):
    #         test = 'Anonymous'
    #         response_post = Client().post('/', {'Statusku': test, 'isi_Status': test})
    #         self.assertEqual(response_post.status_code, 302)
    
    #         response= Client().get('/')
    #         html_response = response.content.decode('utf8')
    #         self.assertNotIn(test, html_response)


    # def test_create_anything_model(self, judul='ini judul', isi='ini isi Status'):
    # 		return Status.objects.create(Statusku = judul, isi_Status=isi)

    # def test_Story6_url_is_exist(self):
    #     response = Client().get('/profile/')
    #     self.assertEqual(response.status_code,200)

    # def test_Story6_using_index_template(self):
    #     response = Client().get('/profile/')
    #     self.assertTemplateUsed(response, 'profile.html')

    # def test_Story6_using_addStatus_func(self):
    #     found = resolve('/profile/')
    #     self.assertEqual(found.func,profile)

# class NewVisitorTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()

#         chrome_options.add_argument('--dns-prefetch-disable')

#         chrome_options.add_argument('--no-sandbox')

#         chrome_options.add_argument('--headless')

#         chrome_options.add_argument('disable-gpu')

#         service_log_path = "./chromedriver.log"

#         service_args = ['--verbose']

#         self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

#         self.browser.implicitly_wait(3) 

#         super(NewVisitorTest,self).setUp()

#     def test_can_start_a_list_and_retrieve_it_later(self):
#         self.browser.get('https://ppw-b-andri-sanss.herokuapp.com/')
#         time.sleep(5) # Let the user actually see something!
#         search_box = self.browser.find_element_by_name('Statusku')
#         search_boxx = self.browser.find_element_by_name('isi_Status')
#         search_box.send_keys('Belajar')
#         search_boxx.send_keys('ppw')
#         search_box.submit()
#         time.sleep(5) # Let the user actually see something!
#         self.assertIn( "Belajar", self.browser.page_source)

#     def test_position_and_css_form(self):
#         self.browser.get('https://ppw-b-andri-sanss.herokuapp.com/')
#         time.sleep(5)
#         delete_status = self.browser.find_element_by_name('Delete_Button')
#         profile_button = self.browser.find_element_by_name('Profile')

#         self.assertIn("Profile", profile_button.get_attribute('name'))
#         self.assertIn("Delete Status", delete_status.text)

#         delete_button_class = "btn-primary" in delete_status.get_attribute("class")
#         profile_button_class = "nav-item" in profile_button.get_attribute("class")

#         self.assertTrue(delete_button_class)
#         self.assertTrue(profile_button_class)


if __name__ == '__main__':
    unittest.main(warnings='ignore')         

# # Create your tests here. 
