from django.http import HttpResponse
from django.shortcuts import render

def blank(request):
	return render(request,'blank.html')

def aboutme(request):
	return render(request,'aboutme.html')

def form(request):
    return render(request,'form.html')	