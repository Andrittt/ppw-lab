from django.contrib import admin
from django.urls import path
from django.http import HttpResponse
from django.conf.urls import url
from Story9 import views as storyviews
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',storyviews.table),

]