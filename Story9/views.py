from django.shortcuts import render
from django.http import JsonResponse
import urllib.request, json
import requests

def table(request):
	return render(request,'Table.html')

def book_data(request):
	try:
		q = request.GET['q']
	except:
		q = 'quilting'

	getJson = requests.get('https://www.googleapis.com/books/v1/volumes?q='+ q)
	jsonparse = json.dumps(getJson.json())
	return HttpResponse(jsonparse)






# Create your views here.
