from django.test import TestCase, Client
from django.urls import resolve
from .views import table

import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story9test(TestCase):
	def test_Story9_url_is_exist(self):
	    response = Client().get('/')
	    self.assertEqual(response.status_code,200)

	def test_Story9_using_index_template(self):
	    response = Client().get('/')
	    self.assertTemplateUsed(response, 'Table.html')

	def test_Story9_using_addStatus_func(self):
	    found = resolve('/')
	    self.assertEqual(found.func,table)

# Create your tests here.
